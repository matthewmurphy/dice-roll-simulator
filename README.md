It simulates rolling a single die and two dice. Displays a bar graph, using 
chart.js showing the frequency of the results of rolling two dice 
one thousand times.