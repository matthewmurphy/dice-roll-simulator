// Rolling 2 dice and returning the probability of each possible roll
// Creates a visual bargraph of the resulting data

// START   

let oneDie = document.getElementById('oneDie');
let twoDice = document.getElementById('twoDice');
let barGraph = document.getElementById('barGraph');

// Represents 1st Die
function myRandom() {
    let upper = 6;
    let lower = 1;
    let myRandomNum = Math.ceil(Math.random() * (upper - lower + 1));
    return myRandomNum
}
myRandom();
let oneDieRoll = document.createTextNode(myRandom());
oneDie.appendChild(oneDieRoll);
// Represents 2nd Die
function myRandom2() {
    let upper = 6;
    let lower = 1;
    let myRandomNum2 = Math.ceil(Math.random() * (upper - lower + 1));
    return myRandomNum2
}
myRandom2();
let twoDiceRoll = document.createTextNode(myRandom2() + myRandom());
twoDice.appendChild(twoDiceRoll);
// Rolls both 1st and 2nd dice and adds them together
// Stores frequency of resulting possible rolls (2-12) into an array
function diceRoll(){
    let count = 0;
    let countArr = [0,0,0,0,0,0,0,0,0,0,0];
    while (count < 1000){
        let result = myRandom() + myRandom2();
        count++
        countArr[result - 2] = countArr[result - 2] + 1;
        console.log(countArr)
        console.log(result)
        
    }
    return countArr
}
diceRoll();

const chartOne = document.getElementById('barChart');

Chart.defaults.global.animation.duration = 200;


let barChart = new Chart(chartOne, {
    type: 'bar',
    data: {
    labels: ['2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
    datasets: [
        {
        label: 'Dice Roll Frequency',
        borderWidth: 2,
        borderColor: '#ff00ff',
        data: diceRoll()
        }
    ]
    }
});

let chartContent = document.createTextNode(barChart);
chartOne.appendChild(chartContent);